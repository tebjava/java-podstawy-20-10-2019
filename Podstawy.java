/*
 * To jest komentarz wieloliniowy
 * czyli taki, który może zajac kilka/kilkanaście linii w naszym kodzie zródłowym
 * Tekst zapisany w komentarzu nie jest brany pod uwage przez kompilator/parser 
 */
// przykład komentarza jednoliniowego; można go rozpoczac w dowolnym momencie jednak konczy sie on wraz z końcem linii, w którym został napisany
// przez co każdorazowo musimy go "otwierac" (napisac sekwencje //)
public class Podstawy {

	public static void main(String[] args) {
		
		/*
		 * Poniżej zaprezentowane zostały zmienne proste (primitives). Zajmuja one 
		 * zawsze określona ilosc miejsca w pamieci RAM i nie posiadaja one
		 * zadnych dodatkowych cech, funkcji czy mozliwosci bezposrednich dzialan
		 * na obiektach
		 */
		
		//zmienna typu prawda/fałsz. Przyjmuje tylko dwie wartości:
		// fałsz (false, binarnie 0) - najczesciej wykorzystuje sie jako oznajmnienie, że
		//                             coś nie wystepuje/zakończyło sie inaczej niz przewidzielismy
		// prawda (true, binarnie 1) - najczesciej wykorzystuje sie jako oznajmienie, że
	    //							  coś wystepuje/wykonało sie poprawnie
		boolean pf = true;
		//zmienna liczbowa - liczba całkowita. Może przyjmowac wartosci zapisywane 
		//na 4 bajtach (32 bity, ok. 4,4 mld, dzielone na polowe - wartosci ujemne i dodatnie).
		//zmienna tego typu pozwala na dzialania matematyczne (artymetyka, trygonometria itp.)
		int liczba = 156;
		//zmienna liczowa - liczba całkowita krótka (połowa wartości int - 16 bit/2bajty)
		short liczbaKrotka = -15;
		//zmienna liczbowa - liczba całkowita dluga (dwukrtonosc int)
		long liczbaDluga = 16789;
		//zmienna liczbowa - liczba zmiennoprzecinkowa (floating point). Możemy zapisywac 
		//liczby ułamkowe na 6-7 miejsc po przecinku (całosc na 4 bajtach)
		float zm = 1.15f;
		//zmienna liczbowa - liczba zmiennoprzecinkowa PODWOJNEJ precyzji (wiekszy zakres 
		//dla wartosci ułamkowej - 15-16 miejsc po przecinku (całosc na 8 bajtach)
		double zm2 = -1.15;
		//zmienna znakowa - pozwala na przechowanie pojedynczego znaku z tablicy ASCII
		//zmienna tak naprawde zawiera liczbe odpowiadajaca kodowi w ASCII
		//zajmuje 1 bajt 
		char znak='A';
		//typ tzw. alias (inna postac char) pozwalajacy na zapis wartosci na jednym bajcie
		byte b = 'n';
		
		/*
		 * Zmienne zlozone - najczesciej tworzone z klas (sa obiektami). Nie zajmuja okreslonej
		 * przestrzeni w pamieci RAM (moga nie zajmowac nic, moga zajmowac nawet kilka/
		 * kilkanascie GB pamieci). Czesto moga posiadac dodatkowa funkcjonalnosc oraz
		 * moga byc zamieniane na inne typy zlozone.
		 */
		
		//zmienna typu znakowego. Pozwala na przechowywanie dowolnej ilosci znakow
		//(ograniczenie wynika ewentualnie jednie ze sepecyfikacji JVM, systemu opracyjnego
		//badz ograniczen sprzetowych)
		String ciagZnakowy = "Dobry jezyk!";
		
		
		
		
		// TODO Auto-generated method stub
		System.out.println("Witaj w świecie Java!");
		System.out.print("Tekst z nastepnej linii");
		
		
		
	}

}
